/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package haar;

import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;

/**
 *
 * @author Szymon
 */
public class Recognizator {
    
    CascadeClassifier classifier;
    private int faceSize;
    MainWindow mw;
    
    public Recognizator(MainWindow mw){
        this.mw = mw;
        this.classifier = new CascadeClassifier();
        classifier.load("resources\\kl1\\haarcascade_frontalcatface.xml");
        if(classifier.empty()){
            System.out.println("Pusty klasyfikator");
        }
        faceSize =0;
    }
    
    public void detect(Mat mat){
        //Resize it
        Mat img = mat.clone();
        Imgproc.resize(img, img, new Size(300,400));
        //Matrix for faces
        MatOfRect faceRects = new MatOfRect();
        Mat imgBW = new Mat();
        //Turn and equalize image to grayscale
        Imgproc.cvtColor(img, imgBW, Imgproc.COLOR_BGR2GRAY);
        Imgproc.equalizeHist(imgBW, imgBW);
        //Get face size
        if(this.faceSize == 0){
            int x = imgBW.rows();
            if(Math.round(x * 0.15f) > 0){
                this.faceSize = Math.round(x * 0.15f);
            }
        }
        //Detection Main Part!!
        this.classifier.detectMultiScale(imgBW, faceRects,1.1,2, Objdetect.CASCADE_SCALE_IMAGE,
                new Size(this.faceSize,this.faceSize), new Size());
        
        Rect[] faces = faceRects.toArray();
        for(Rect r : faces){
            Imgproc.rectangle(img, r.tl(), r.br(), new Scalar(0,255,0),3);
        }//Nothing was detected
        if(faces.length == 0){
            System.out.println("Pusto");
        }//Paint image with rectangles on main window
        this.mw.changeIMGOnLabel(img);
    }
    
    public boolean setNewClassifier(String path){
        CascadeClassifier cc = classifier;
        classifier.load(path);
        if(!classifier.empty())
            return true;
        else{
            classifier = cc;
            return false;
        }
    }
}
