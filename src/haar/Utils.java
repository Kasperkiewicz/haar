package haar;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import org.opencv.core.Mat;

public class Utils {
	public static Image matToBImage(Mat mat) {
		 BufferedImage bufImage;
		 int type = BufferedImage.TYPE_BYTE_GRAY;
		 if(mat.channels() > 1) {
			 type = BufferedImage.TYPE_3BYTE_BGR;
		 }
		 
		 bufImage = new BufferedImage(mat.cols(), mat.rows(),type);
		 mat.get(0,0,((DataBufferByte)bufImage.getRaster().getDataBuffer()).getData());
		 return bufImage;              
	}
	
}
